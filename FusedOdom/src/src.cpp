/*
 * Copyright (C) 2008, Morgan Quigley and Willow Garage, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of Stanford University or Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "rtabmap_ros/Graph.h"
//#include "rtabmap_ros/Path.h"
//#include "rtabmap_ros/MsgConversion.h"
/**
 * This tutorial demonstrates subscribing to a topic using a class method as the callback.
 */

// %Tag(CLASS_WITH_DECLARATION)%
class Listener
{
public:
  void callbackLine(const std_msgs::String::ConstPtr& msg);
  void callbackRtabMap(const rtabmap_ros::Graph::ConstPtr& msg);
};
// %EndTag(CLASS_WITH_DECLARATION)%

void Listener::callbackLine(const std_msgs::String::ConstPtr& msg)
{

std::cout << msg->data.c_str() << std::endl;

}

void Listener::callbackRtabMap(const rtabmap_ros::Graph::ConstPtr& msg)
{

    for (size_t i = 0; i < msg->poses.size(); ++i)
    {
   std::cout << " PosRtabMap " << msg->poses[i].position.x << " " << msg->poses[i].position.y << " " << msg->poses[i].position.z << " " <<
	msg->poses[i].orientation.x << " " << msg->poses[i].orientation.y << " " << msg->poses[i].orientation.z << " " << msg->poses[i].orientation.w << std::endl;

//std::cout << msg->poses[i].position.x << std::endl;
    }

}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener_class");
  ros::NodeHandle n;

// %Tag(SUBSCRIBER)%
  Listener listener;
  ros::Subscriber sub = n.subscribe("/lineslam/StringOdom", 1000, &Listener::callbackLine, &listener);
  ros::Subscriber sub2 = n.subscribe("/rtabmap/graph", 1000, &Listener::callbackRtabMap, &listener);
// %EndTag(SUBSCRIBER)%

  ros::spin();

  return 0;
}
